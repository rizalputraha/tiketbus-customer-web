#!/bin/bash

# any future command that fails will exit the script
set -e

cd /home/islam/customer

git pull git@gitlab.com:b36/tiketbus-customer-web.git master

# install npm packages
npm install

npm run build

cd public/build
rm -r bundle.css.map
rm -r bundle.js.map

# Restart the node server
source ~/.profile

pm2 restart customer