// auth.js
import { writable } from 'svelte/store';
import jwt from 'jwt-decode';

let _user = localStorage.getItem('token');
export const store = writable(_user ? JSON.parse(_user) : null);
export const userData = writable(null);
store.subscribe((value) => {
    if (value) {
        localStorage.setItem('token', JSON.stringify(value));
        userData.set(jwt(value.token.data));
    }
    else {
        localStorage.removeItem('token'); // for logout   
    }
});
