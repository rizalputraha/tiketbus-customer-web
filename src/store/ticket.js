// auth.js
import { writable } from 'svelte/store';

const tiket = localStorage.getItem('listTicket')
export const listTiket = writable(tiket ? JSON.parse(tiket) : null);
listTiket.subscribe((value) => {
    if (value) {
        localStorage.setItem('listTicket', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('listTicket'); // for logout   
    }
});

const tiketP = localStorage.getItem('listTicketPulang')
export const listTiketPulang = writable(tiketP ? JSON.parse(tiketP) : null);
listTiketPulang.subscribe((value) => {
    if (value) {
        localStorage.setItem('listTicketPulang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('listTicketPulang'); // for logout   
    }
});

const selectedBus = localStorage.getItem('selectedBus')
export const selectBus = writable(selectedBus ? JSON.parse(selectedBus) : null);
selectBus.subscribe((value) => {
    if (value) {
        localStorage.setItem('selectedBus', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('selectedBus'); // for logout   
    }
});

const selectedBusP = localStorage.getItem('selectedBusPulang')
export const selectBusPulang = writable(selectedBusP ? JSON.parse(selectedBusP) : null);
selectBusPulang.subscribe((value) => {
    if (value) {
        localStorage.setItem('selectedBusPulang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('selectedBusPulang'); // for logout   
    }
});

const kursi = localStorage.getItem('selectKursi')
export const selectKursi = writable(kursi ? JSON.parse(kursi) : null);
selectKursi.subscribe((value) => {
    if (value) {
        localStorage.setItem('selectKursi', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('selectKursi'); // for logout   
    }
});

const kursiP = localStorage.getItem('selectKursiPulang')
export const selectKursiPulang = writable(kursiP ? JSON.parse(kursiP) : null);
selectKursiPulang.subscribe((value) => {
    if (value) {
        localStorage.setItem('selectKursiPulang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('selectKursiPulang'); // for logout   
    }
});

const jt = localStorage.getItem('jamtrayek')
export const selectJamTrayek = writable(jt ? JSON.parse(jt) : null);
selectJamTrayek.subscribe((value) => {
    if (value) {
        localStorage.setItem('jamtrayek', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('jamtrayek'); // for logout   
    }
});

const jtPulang = localStorage.getItem('jamtrayekPulang')
export const selectJamTrayekPulang = writable(jtPulang ? JSON.parse(jtPulang) : null);
selectJamTrayekPulang.subscribe((value) => {
    if (value) {
        localStorage.setItem('jamtrayekPulang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('jamtrayekPulang'); // for logout   
    }
});

const tgl = localStorage.getItem('tgl')
export const tglTiket = writable(tgl ? JSON.parse(tgl) : null);
tglTiket.subscribe((value) => {
    if (value) {
        localStorage.setItem('tgl', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('tgl'); // for logout   
    }
});

const tglPulang = localStorage.getItem('tglPulang')
export const tglTiketPulang = writable(tglPulang ? JSON.parse(tglPulang) : null);
tglTiketPulang.subscribe((value) => {
    if (value) {
        localStorage.setItem('tglPulang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('tglPulang'); // for logout   
    }
});

const dataCust = localStorage.getItem('dataDiri')
export const dataDiri = writable(dataCust ? JSON.parse(dataCust) : null);
dataDiri.subscribe((value) => {
    if (value) {
        localStorage.setItem('dataDiri', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('dataDiri'); // for logout   
    }
});

const dataCustPulang = localStorage.getItem('dataDiriPulang')
export const dataDiriPulang = writable(dataCustPulang ? JSON.parse(dataCustPulang) : null);
dataDiriPulang.subscribe((value) => {
    if (value) {
        localStorage.setItem('dataDiriPulang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('dataDiriPulang'); // for logout   
    }
});

const penumpang = localStorage.getItem('jml_penumpang')
export const jmlPenumpang = writable(penumpang ? JSON.parse(penumpang) : null);
jmlPenumpang.subscribe((value) => {
    if (value) {
        localStorage.setItem('jml_penumpang', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('jml_penumpang'); // for logout   
    }
});

const isPulangPergi = localStorage.getItem('pulangPergi')
export const pulangPergi = writable(isPulangPergi ? JSON.parse(isPulangPergi) : null);
pulangPergi.subscribe((value) => {
    if (value) {
        localStorage.setItem('pulangPergi', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('pulangPergi'); // for logout   
    }
});

const total = localStorage.getItem('total');
export const totalHarga = writable(total ? JSON.parse(total) : null);
totalHarga.subscribe((value) => {
    if (value) {
        localStorage.setItem('total', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('total'); // for logout   
    }
});

const pesanTiket = localStorage.getItem('dataPesanTiket');
export const dataPesanTiket = writable(pesanTiket ? JSON.parse(pesanTiket) : null);
dataPesanTiket.subscribe((value) => {
    if (value) {
        localStorage.setItem('dataPesanTiket', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('dataPesanTiket'); // for logout   
    }
});

const bankPilihan = localStorage.getItem('bank');
export const selectBank = writable(bankPilihan ? JSON.parse(bankPilihan) : null);
selectBank.subscribe((value) => {
    if (value) {
        localStorage.setItem('bank', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('bank'); // for logout   
    }
});

const noVa = localStorage.getItem('no_va');
export const nomorVa = writable(noVa ? JSON.parse(noVa) : null);
nomorVa.subscribe((value) => {
    if (value) {
        localStorage.setItem('no_va', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('no_va'); // for logout   
    }
});

const idTrans = localStorage.getItem('id_transaksi');
export const idTransaksi = writable(idTrans ? JSON.parse(idTrans) : null);
idTransaksi.subscribe((value) => {
    if (value) {
        localStorage.setItem('id_transaksi', JSON.stringify(value));
    }
    else {
        localStorage.removeItem('id_transaksi'); // for logout   
    }
});