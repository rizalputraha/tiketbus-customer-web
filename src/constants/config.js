import { baseUrl, imgUrl } from '../env';

export const BaseUrl = baseUrl;
export const ImgBaseUrl = imgUrl;
export const HttpTimeOut = 60000;
export const firebaseConfig = {
    apiKey: "AIzaSyAONpWmd_3COCC5YkqUxZyjY_IygQrwoSo",
    authDomain: "e-ticket-289307.firebaseapp.com",
    databaseURL: "https://e-ticket-289307.firebaseio.com",
    projectId: "e-ticket-289307",
    storageBucket: "e-ticket-289307.appspot.com",
    messagingSenderId: "975432108290",
    appId: "1:975432108290:web:3bdc93c12b12564a28da1b",
};