import { appGet, appPost } from '../caller';

const createKursi = async (token, data) => {
    let url = 'api/v1/kursi/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listKursi = async (token) => {
    let url = 'api/v1/kursi/bus_kursi'
    let resp = appGet({ url, token });
    return resp;
}

const updateKursi = async (token, id, data) => {
    let url = `api/v1/kursi/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteKursi = async (token, id) => {
    let url = `api/v1/kursi/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createKursi, updateKursi, deleteKursi, listKursi };