import { appGet, appPost } from '../caller';

const registerMember = async (token, data) => {
    let url = 'api/v1/member/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const updateProfil = async (token,data,id) => {
    let url = `/api/v1/member/update/${id}`;
    let resp = appPost({url,data,token});
    return resp;
}

export default { registerMember,updateProfil };