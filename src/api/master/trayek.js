import { appGet, appPost } from '../caller';

const createTrayek = async (token, data) => {
    let url = 'api/v1/trayek/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listTrayek = async (token) => {
    let url = 'api/v1/trayek/read'
    let resp = appGet({ url, token });
    return resp;
}

const updateTrayek = async (token, id, data) => {
    let url = `api/v1/trayek/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteTrayek = async (token, id) => {
    let url = `api/v1/trayek/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createTrayek, listTrayek, updateTrayek, deleteTrayek };