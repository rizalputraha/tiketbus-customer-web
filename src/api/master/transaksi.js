import { appGet, appPost } from '../caller';

const listTiket = async (token, data) => {
    let url = `api/v1/tiket/cek_tiket?asal=${data.asal}&tujuan=${data.tujuan}`
    let resp = appGet({ url, data, token });
    return resp;
}

const listTiketCust = async (token, data) => {
    let url = `api/v1/tiket/cek_tiket/no_auth?asal=${data.asal}&tujuan=${data.tujuan}&tanggal=${data.tgl}`
    let resp = appGet({ url, data, token });
    return resp;
}

const transAgen = async (token) => {
    let url = 'api/v1/transagen/read'
    let resp = appGet({ url, token });
    return resp;
}

const createTransAgen = async (token, data) => {
    let url = 'api/v1/transagen/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const updateTrans = async (token, id, data) => {
    let url = `api/v1/transagen/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const updateLunas = async (token, id) => {
    let url = `api/v1/transagen/lunas/${id}`
    let resp = appPost({ url, token });
    return resp;
}

const listTransCust = async (token) => {
    let url = `api/v1/transcust/list_transaction`;
    let resp = appGet({ url, token });
    return resp;
}

const transCust = async (token, data) => {
    let url = `api/v1/transcust/transaction`;
    let resp = appPost({ url, data, token });
    return resp;
}

export default { listTiket, transAgen, createTransAgen, updateTrans, updateLunas, transCust, listTiketCust, listTransCust };