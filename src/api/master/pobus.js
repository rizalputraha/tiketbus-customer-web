import { appGet, appPost } from '../caller';

const createPoBus = async (token, data) => {
    let url = 'api/v1/pobus/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listPoBus = async (token) => {
    let url = 'api/v1/pobus/read'
    let resp = appGet({ url, token });
    return resp;
}

const updatePoBus = async (token, id, data) => {
    let url = `api/v1/pobus/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const historiPoAgen = async (token) => {
    let url = 'api/v1/historipo/agen';
    let resp = appGet({ url, token });
    return resp;
}

const historiPoCrew = async (token) => {
    let url = 'api/v1/historipo/crew';
    let resp = appGet({ url, token });
    return resp;
}

const deletePoBus = async (token, id) => {
    let url = `api/v1/pobus/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createPoBus, listPoBus, updatePoBus, deletePoBus, historiPoAgen, historiPoCrew };