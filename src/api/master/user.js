import { appGet, appPost } from '../caller';

const createUser = async (token, data) => {
    let url = 'api/v1/user/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listUser = async (token) => {
    let url = 'api/v1/user/list'
    let resp = appGet({ url, token });
    return resp;
}

const updateUser = async (token, id, data) => {
    let url = `api/v1/user/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteUser = async (token, id) => {
    let url = `api/v1/user/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

const listEmployee = async (token) => {
    let url = `api/v1/pobus/list-employe`
    let resp = appGet({ url, token });
    return resp;
}

export default { createUser, listUser, updateUser, deleteUser, listEmployee };