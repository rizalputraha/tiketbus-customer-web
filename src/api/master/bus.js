import { appPost, appGet } from '../caller';

const createBus = async (token, data) => {
    const url = 'api/v1/bus/create';
    let resp = await appPost({ url, data, token });
    return resp;
}

const listBus = async (token, data) => {
    const url = 'api/v1/bus/read';
    let resp = await appGet({ url, token });
    return resp;
}

const updateBus = async (token, id, data) => {
    const url = `api/v1/bus/update/${id}`;
    let resp = await appPost({ url, data, token });
    return resp;
}

const deleteBus = async (token, id) => {
    const url = `api/v1/bus/delete/${id}`;
    let resp = await appPost({ url, token });
    return resp;
}

export default { createBus, listBus, updateBus, deleteBus };