import { appGet, appPost } from '../caller';

const createAgen = async (token, data) => {
    let url = 'api/v1/agen/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listAgen = async (token) => {
    let url = 'api/v1/agen/read'
    let resp = appGet({ url, token });
    return resp;
}

const updateAgen = async (token, id, data) => {
    let url = `api/v1/agen/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteAgen = async (token, id) => {
    let url = `api/v1/agen/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createAgen, listAgen, updateAgen, deleteAgen };