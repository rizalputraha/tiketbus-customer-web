import { appGet, appPost } from '../caller';

const listBank = async (token, data) => {
    const url = 'api/v1/bank/read';
    let resp = await appGet({ url, data, token });
    return resp;
}

const checkStatus = async (token, data) => {
    const url = `api/v1/check/get/briva/J104408/77777/${data.no_va}`;
    let headers = {
        'Authorization': `Bearer ${data.access_token}`
    }
    let resp = await appGet({ url, data, token, headers });
    return resp;
}

const checkPayment = async (token,data) => {
    const url = `/api/v1/transcust/cek_transaction`
    let resp = await appPost({ url, token, data});
    return resp;
}

const getCredential = async (token, data) => {
    const url = `api/v1/check/modulbri?grant_type=client_credentials`;
    let resp = await appPost({ url, data, token });
    return resp;
}

export default { listBank, checkStatus, getCredential,checkPayment };