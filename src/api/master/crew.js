import { appGet, appPost } from '../caller';

const createCrew = async (token, data) => {
    let url = 'api/v1/crew/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listCrew = async (token) => {
    let url = 'api/v1/crew/read'
    let resp = appGet({ url, token });
    return resp;
}

const updateCrew = async (token, id, data) => {
    let url = `api/v1/crew/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteCrew = async (token, id) => {
    let url = `api/v1/crew/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createCrew, listCrew, updateCrew, deleteCrew };