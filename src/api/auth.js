import { appPost } from './caller';

const loginReq = async (data) => {
    // let { email, password } = data;
    let url = 'api/v1/user/login';
    let resp = await appPost({ data, url });
    return resp;
}

const loginMember = async (data) => {
    let url = 'api/v1/member/login';
    let resp = await appPost({ data, url });
    return resp;
}

export default { loginReq, loginMember };