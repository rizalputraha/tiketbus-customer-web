import axios from 'axios';
import { BaseUrl } from '../constants/config';

const Call = async (method, url, data = {}, token = '', headers = {}, options = {}, baseUrl = BaseUrl, timeout = 60000) => {
    const config = {
        ...options,
        method,
        url,
        baseURL: baseUrl,
        data,
        timeout,
        headers: { ...headers, "npc-token": token }
    }
    // console.log({config});
    return await axios.request(config)
}

export const appPost = async (body) => {
    const { url, data, token, headers, baseUrl, timeout } = body;
    // console.log({body});
    return await Call('POST', url, data, token, headers, baseUrl, timeout);
}

export const appGet = async (body) => {
    const { url, data, token, headers, baseUrl, timeout } = body;
    const params = JSON.stringify(data);
    return await Call('GET', url, params, token, headers, baseUrl, timeout);
}

export const appDelete = async (body) => {
    const { url, data, token, headers, baseUrl, timeout } = body;
    return await Call('DELETE', url, data, token, headers, baseUrl, timeout);
}

export const appPatch = async (body) => {
    const { url, data, token, headers, baseUrl, timeout } = body;
    return await Call('PATCH', url, data, token, headers, baseUrl, timeout);
}