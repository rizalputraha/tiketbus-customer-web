//  payload :
//     nm_member,
//     pass_user,
//     cpass_user,
//     email_user,
//     telp_usr,
//     addr_member
// 
const validateRegister = (param) => {
    if (param.nm_member == '') {
        return 'Kolom Nama Tidak Boleh Kosong';
    }
    if (param.email_user == '') {
        return 'Kolom Email Tidak Boleh Kosong';
    }
    if (param.pass_user == '') {
        return 'Kolom Password Tidak Boleh Kosong';
    }
    if (param.cpass_user == '') {
        return 'Kolom Konfirmasi Tidak Boleh Kosong';
    }
    if (param.telp_user == '') {
        return 'Kolom No. Telp Tidak Boleh Kosong';
    }
    if (param.addr_member == '') {
        return 'Kolom Alamat Tidak Boleh Kosong';
    }
    if (param.cpass_user != param.pass_user) {
        return "Password Tidak Sama"
    }
    return true;
}

//  payload :
//     email,
//     password,
// 
const validateLogin = (param) => {
    if (param.email == '') {
        return 'Kolom Email Tidak Boleh Kosong';
    }
    if (param.password == '') {
        return 'Kolom Password Tidak Boleh Kosong';
    }
    return true;
}

const validatePesanTiket = (param) => {
    if (param.asal == '#') {
        return 'Kolom Kota Asal Tidak Boleh Kosong';
    }
    if (param.tujuan == '#') {
        return 'Kolom Kota Tujuan Tidak Boleh Kosong';
    }
    if (param.tgl == '') {
        return 'Kolom Tanggal Tidak Boleh Kosong';
    }
    return true;
}

const validateDataDiri = (param) => {
    for (let i = 0; i < param.length; i++) {
        const item = param[i];
        if (item.nama == "") {
            return 'Kolom Nama Tidak Boleh Kosong';
        }
        if (item.email == "") {
            return 'Kolom Email Tidak Boleh Kosong';
        }
        if (item.telp == "") {
            return 'Kolom Telp Tidak Boleh Kosong';
        }
        return true;
    }
}

module.exports = {
    validateRegister,
    validateLogin,
    validatePesanTiket,
    validateDataDiri
}